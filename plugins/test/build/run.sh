#!/bin/bash

function clean {
    rm -f Makefile
    rm -f CMakeCache.txt
    rm -f cmake_install.cmake
    rm -rf CMakeFiles
    rm -f lmp_test.so
}

clean
cmake CMakeLists.txt
make
rm -f ~/Desktop/hlds/valve/addons/lambdamod/dlls/lmp_test.so
cp lmp_test.so ~/Desktop/hlds/valve/addons/lambdamod/dlls/
clean
