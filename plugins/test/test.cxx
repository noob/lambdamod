//#include "CHyperCube.hxx"
#include <iostream>

// /////////////////////////////////////////////////////////////////////////////
// [export API]: Begin
// /////////////////////////////////////////////////////////////////////////////

void lambdamod_init(size_t nStartPhase) {
    std::cout << "\n << lambdamod_init ('test' plugin) >>" << std::endl;
}

extern "C" void lambdamod_start_phase(size_t nStartPhase) {
    lambdamod_init(nStartPhase);
}

extern "C" void lmp_hypercube_import_trackEventByEntTag(void *) {
    std::cout << "\nlmp_hypercube_import_trackEventByEntTag" << std::endl;
    //using TRACK_FPTR_T = size_t (*)(const char *, size_t, void *, size_t);
    //TRACK_FPTR_T fptr = &CHyperCube::trackEvent;
    //return fptr;
} // lmm_trackEventByEntTag_export

extern "C" void lmp_hypercube_import_trackEventByEntId(void *) {
std::cout << "\nlmp_hypercube_import_trackEventByEntId" << std::endl;
    //using TRACK_FPTR_T = size_t (*)(size_t, size_t, void *, size_t);
    //TRACK_FPTR_T fptr = &CHyperCube::trackEvent;
    //return fptr;
} // lmm_trackEventByEntId_export

extern "C" void lmp_hypercube_import_hangTracking(void * fptr) {
    using FPTR_CB = void (*)(size_t);
    
    std::cout << "\nlmp_hypercube_import_hangTracking" << std::endl;
    ((FPTR_CB)fptr)(0);
    
    //return &CHyperCube::hangTracking;
} // lmm_hangTracking_export

extern "C" void lmp_hypercube_import_unhangTracking(void * fptr) {
    using FPTR_CB = void (*)(size_t);
    
    std::cout << "\nlmp_hypercube_import_unhangTracking" << std::endl;
    ((FPTR_CB)fptr)(0);
    //return &CHyperCube::unhangTracking;
} // lmm_unhangTracking_export

extern "C" void lmp_hypercube_import_isTracked(void *) {
std::cout << "\nlmp_hypercube_import_isTracked" << std::endl;
    //return &CHyperCube::isTracked;
} // lmm_isTracked_export
