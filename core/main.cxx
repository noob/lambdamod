#include "extdll.h"
#include "meta_api.h"
#include "globals.hxx"
#include "CExtensionsLoader.hxx"
#include "CHostInfoProvider.hxx"

////////////////////////////////////////////////////////////////////////////////

#define PLUGIN_UNIQUE "lambdamod_core"
#define PLUGIN_LOAD PT_STARTUP
#define PLUGIN_UNLOAD PT_NEVER // TODO
#define PLUGIN_AUTHOR "Turanga_Leela"
#define PLUGIN_URL "aghl.ru"
#define PLUGIN_VERSION "0.01"

// 'amxx' func's orded .. to remember
// natives
// precache
// init
// cfg

////////////////////////////////////////////////////////////////////////////////

void
gamedllFunc_srvActivate_POST ( edict_t *,	// pEdictList
                              int32_t, 	// iEdictsCount
                              int32_t 	// iClientsMax
                            )
{
    std::cout << "\n[10.06.2019]::srvActivatePost" << std::endl;
    
    lambdamod::g_hostInfoProvider.init();
    const auto bResult = lambdamod::g_extensionsLoader.loadLibs();
    if(bResult) {
        auto && startPhaseFptrs =
            lambdamod::g_extensionsLoader.getStartPhaseFptrs();
        for(auto fptr : startPhaseFptrs) {
            fptr(0);
        }
    }

    std::cout << "\n[LAMBDAMOD]: gamePath="
        << lambdamod::g_hostInfoProvider.getPath() << std::endl;
    
    (gpMetaGlobals -> mres) = MRES_IGNORED;
}

////////////////////////////////////////////////////////////////////////////////

int
gamedllFunc_attachEntityAPI2_POST ( DLL_FUNCTIONS * pFunctionTable,
                                    int32_t * interfaceVersion
                                  )
{
    pFunctionTable->pfnServerActivate = &gamedllFunc_srvActivate_POST;

    return 0;
}

////////////////////////////////////////////////////////////////////////////////

C_DLLEXPORT void
GiveFnptrsToDll(enginefuncs_t * pEngFuncs, globalvars_t * pGlobals) {
    memcpy(&g_engfuncs, pEngFuncs, sizeof(enginefuncs_t));
    gpGlobals = pGlobals;
}

////////////////////////////////////////////////////////////////////////////////

C_DLLEXPORT int
Meta_Query( char * ifVer, plugin_info_t * pinfo[],
            mutil_funcs_t * pMetaUtilFuncs)
{
    if(nullptr == pinfo || nullptr == pMetaUtilFuncs) {
        return -1;
    }

    g_pluginInfo.date = (char *)"June 2019";
    g_pluginInfo.author = (char *)PLUGIN_AUTHOR;
    g_pluginInfo.ifvers = ifVer;
    g_pluginInfo.loadable = PLUGIN_LOAD;
    g_pluginInfo.logtag = (char *)"hl forever!";
    g_pluginInfo.name = (char *)PLUGIN_UNIQUE;
    g_pluginInfo.unloadable = PLUGIN_UNLOAD;
    g_pluginInfo.url = (char *)PLUGIN_URL;
    g_pluginInfo.version = (char *)PLUGIN_VERSION;

    gpPluginInfo = &g_pluginInfo;
    *pinfo = gpPluginInfo;
    gpMetaUtilFuncs = pMetaUtilFuncs;

    return 1;
}

////////////////////////////////////////////////////////////////////////////////

C_DLLEXPORT int
Meta_Attach (   PLUG_LOADTIME now,
                META_FUNCTIONS * pMetaFuncsTable,
                meta_globals_t * pMetaGlobalVars,
                gamedll_funcs_t * pGameDllFuncs )
{
    if ( nullptr == pMetaFuncsTable || nullptr == pMetaGlobalVars ||
         nullptr == pGameDllFuncs )
    {
        return -1;      
    }

    g_metaFuncs.pfnGetEntityAPI2_Post = &gamedllFunc_attachEntityAPI2_POST;
    //g_metaFuncs.pfnGetEntityAPI = ..;
    //g_metaFuncs.pfnGetEntityAPI_Post = ..;
    //g_metaFuncs.pfnGetEntityAPI2 = ..;
    //g_metaFuncs.pfnGetEntityAPI2_Post = ..;
    //g_metaFuncs.pfnGetNewDLLFunctions = ..;
    //g_metaFuncs.pfnGetNewDLLFunctions_Post = ..;
    //g_metaFuncs.pfnGetEngineFunctions = ..;
    //g_metaFuncs.pfnGetEngineFunctions_Post = ..;

    memcpy( pMetaFuncsTable, &g_metaFuncs, sizeof(META_FUNCTIONS));

    gpMetaGlobals = pMetaGlobalVars;
    gpGamedllFuncs = pGameDllFuncs;

    return 1;
}

////////////////////////////////////////////////////////////////////////////////

