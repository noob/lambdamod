#pragma once

#include <iostream>
#include <string_view>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <vector>
#include <sstream>
#include <set>
#include <filesystem>
#include <map> 
#include <regex>
#include <dlfcn.h>
#include "CLogger.hxx"
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <string>

template< size_t N >
constexpr size_t length( char const (&)[N] )
{
    return N-1;
}

namespace lambdamod {

class CExtensionsLoader {
private:
    // contains short names as keys, like 'hypercube'
    // 1st value - address of a dll
    // 2nd value - address of "lambdamod_start_phase" in a related dll
    std::unordered_map<std::string, std::pair<void *, void *>> m_dlls;

////////////////////////////////////////////////////////////////////////////////

    auto libLoadAPI (
        const std::string & sLibPath,
        const std::string & sPrefix,
        const std::string & sExt,
        const std::string & sLibName ) {

        auto lambdaMakeLibAPIListName = [&sLibPath, &sExt]() -> auto {
            std::string sRet;
            sRet.reserve(256);

            sRet += sLibPath;
            sRet += '.';
            sRet += sExt;

            sRet.shrink_to_fit();
            return sRet; 
        }; // lambdaMakeLibAPIListName

        auto && sLibAPIListName = lambdaMakeLibAPIListName();

        auto lambdaMakeLibParsingCMD =
            [&sLibPath, &sExt, &sPrefix, &sLibAPIListName] () -> auto {

            std::string sRet;
            sRet.reserve(256 + 64);

            sRet += "objdump -x ";
            sRet += sLibPath;
            sRet += " | egrep '\\s+";
            sRet += sPrefix;
            sRet += "_\\w+_";
            sRet += sExt;
            sRet += "_' | awk -F ' ' '{ print $6 }' > ";
            sRet += sLibAPIListName;

            sRet.shrink_to_fit();
            return sRet;
        }; // lambdaMakeLibParsingCMD
        
        auto && sParsingCMD = lambdaMakeLibParsingCMD();
        const auto cmdResult = system(sParsingCMD.c_str());
        std::string sLine;
        std::unordered_map<std::string, void *> apiMethods;
    
        if(0 != cmdResult) {
            g_logger.appendAsError("can't execute 'syscmd' :: < " +
                                   sParsingCMD + " >", 6);
            return decltype(apiMethods){};
        }
    
        std::ifstream moduleAPI (sLibAPIListName.c_str(), std::ios_base::in);

        if (moduleAPI.good()) {
            auto dl = dlopen(sLibPath.c_str(), RTLD_LAZY);
            if(nullptr == dl) {
                g_logger.appendAsWarning("'dlopen' returned 'nullptr'", 6);
                return decltype(apiMethods){};
            }

            auto lambdaReadDll =
                [](void * pDll, const std::string & sWord) -> void * {

                auto funcAddr = dlsym(pDll, sWord.c_str());
                if(nullptr == funcAddr) {
                    g_logger.appendAsWarning(   "'dlsym' has not found '"
                                                + sWord + "'", 6);
                    dlclose(pDll);
                    return nullptr;
                }

                return funcAddr;
            }; // lambdaReadDll

            while (getline(moduleAPI, sLine)) {
                auto funcAddr = lambdaReadDll(dl, sLine);
                if(nullptr == funcAddr) {
                    return decltype(apiMethods){};
                }

                apiMethods[sLine] = funcAddr;
            }

            auto pStartPhaseFptr = lambdaReadDll(dl, "lambdamod_start_phase");
            if(nullptr == pStartPhaseFptr) {
                return decltype(apiMethods){};
            }

            m_dlls[sLibName] = std::make_pair(dl, pStartPhaseFptr);
        } else {
            g_logger.appendAsWarning("can't read: '" + sLibAPIListName +
                "'", 6);

            return decltype(apiMethods){};
        }
    
        return apiMethods;
    } // libLoadAPI

////////////////////////////////////////////////////////////////////////////////

    auto loadLibs ( const std::string & sDir,
                    const std::string & sPrefix,
                    const std::string & sExt ) {

        std::regex reData ( [](const std::string & sWord) -> auto {
            std::string sRet;
            sRet.reserve(32);

            sRet += ".*/";
            sRet += sWord;
            sRet += "_\\w+\\.so";
            
            sRet.shrink_to_fit();
            return sRet; } (sPrefix) );

        std::smatch reSmatch;
        std::unordered_set<std::string> blackList;

        std::unordered_map< std::string,
                            std::unordered_map<std::string, void *>> apiFuncs;

        auto lambdaGetLibName =
            [](const std::string & sFullPath) -> std::string {
            
            auto nEndOffset = sFullPath.rfind('.');
            if(std::string::npos == nEndOffset) {
                return "";
            }

            auto nBeginOffset = sFullPath.rfind('/');
            if(std::string::npos == nBeginOffset) {
                return "";
            }
            ++nBeginOffset;

            std::string sTmpName (  nBeginOffset + sFullPath.c_str(),
                                    nEndOffset + sFullPath.c_str()    );

            nBeginOffset = sTmpName.find('_');
            if(std::string::npos == nBeginOffset) {
                return "";
            }
            ++nBeginOffset;

            return {    sTmpName.c_str() + nBeginOffset,
                        sTmpName.c_str() + sTmpName.length()    };
        }; // lambdaGetLibName

        auto lambdaMakeFullLibName = [] (
            std::string_view sBegin,
            std::string_view sName,
            const char * pszInfo = nullptr ) -> auto {

            std::string sRet;
            sRet.reserve(256 + 64);

            sRet += '\'';
            sRet += sBegin;
            sRet += '_';
            sRet += sName;
            
            if(pszInfo) {
                sRet += pszInfo;
            }

            sRet.shrink_to_fit();
            return sRet;               
        }; // lambdaMakeFullLibName

        namespace fs = std::filesystem;
        for (const auto & entry : fs::recursive_directory_iterator(sDir)) {
            std::string sPath = entry.path();
       
            if(std::regex_match(sPath, reSmatch, reData)) {
                auto & rsSmatch = reSmatch[0];
                const std::string sFullLibPath = rsSmatch;

                auto && sLibName = lambdaGetLibName(sFullLibPath);
                if(!sLibName.length()) {
                    continue;
                }

                auto lambdaFullLibNameConstructor =
                    [&sPrefix, &sLibName, &lambdaMakeFullLibName]
                    (const char * pszInfo) -> auto {

                    return lambdaMakeFullLibName(sPrefix, sLibName, pszInfo);
                }; // lambdaFullLibNameConstructor

                auto itBlackList = blackList.find(sLibName);
                if(blackList.end() != itBlackList) {
                    g_logger.appendAsWarning ( 
                        lambdaFullLibNameConstructor (
                            ".so' is already in 'bl'; ignoring .." ), 4 );

                    continue;
                }

                auto it = apiFuncs.find(sLibName);
                if(apiFuncs.end() == it) {
                    g_logger.appendAsInfo ( 
                        lambdaFullLibNameConstructor(".so' is being parsed .."),
                        4 );

                    auto tmpApiFuncsList = libLoadAPI (
                        sFullLibPath, sPrefix, sExt, sLibName );

                    if(tmpApiFuncsList.empty()) {
                        g_logger.appendAsWarning ( 
                            lambdaFullLibNameConstructor (
                                ".so' 0 or not all of expected API funcs. "
                                "have been found; moving to 'bl' .."), 6 );

                        blackList.insert(sLibName);
                        continue;
                    }

                    // TODO         
                    //  const std::string sUnloadFunc = 
                    // auto unloadFuncIt =
                    //  tmpApiFuncsList.find("lambdamod_unload");
                    
                    //  if(tmpApiFuncsList.end() == unloadFuncIt) {
                    //   g_logger.appendAsWarning("'" + sLibName +
                    //    "'unload' func has not been found; "
                    //   "moving to 'bl' ..", 4);
                        
                    //  blackList.insert(sLibName);
                    //  continue;
                        //  }

                    std::string sInfo;
                    sInfo.reserve(64 + 32);

                    sInfo += "'*.so' has been successfully parsed (";
                    sInfo += std::to_string(tmpApiFuncsList.size());
                    sInfo += " API '";
                    sInfo += sExt;
                    sInfo += "' funcs.)";

                    sInfo.shrink_to_fit();

                    apiFuncs[sLibName] = std::move(tmpApiFuncsList);

                    g_logger.appendAsInfo (sInfo.c_str(), 6);
                } else {
                    g_logger.appendAsWarning ( 
                        lambdaFullLibNameConstructor ( 
                        ".so' has already been parsed; data related to both "
                        "the found modules will be ignored; moving to 'bl' .."),
                        4 );

                    apiFuncs.erase(it);
                    blackList.insert(sLibName);
                }
            }
        }

        return apiFuncs;
    } // loadLibs

////////////////////////////////////////////////////////////////////////////////

    auto loadPlugins() {
        return loadLibs("./", "lmp", "import");
    } // loadPlugins

////////////////////////////////////////////////////////////////////////////////

    auto loadModules() {
        return loadLibs("./", "lmm", "export");
    } // loadModules

////////////////////////////////////////////////////////////////////////////////

    static auto makeAPIFuncName (
        const std::string & sModule, const std::string & sFunc, 
        const char * pszPrefix, const char * pszExt ) {

        std::string sRet;
        sRet.reserve(64 + 32);

        sRet += pszPrefix;
        sRet += sModule;
        sRet += pszExt;
        sRet += sFunc;
        
        sRet.shrink_to_fit();
        return sRet;
    } // makeAPIFuncName

////////////////////////////////////////////////////////////////////////////////

    static auto makeImportFuncName (
        const std::string & sModule, const std::string & sFunc ) {

        return makeAPIFuncName(sModule, sFunc, "lmp_", "_import_");
    } // makeImportFuncName
    
////////////////////////////////////////////////////////////////////////////////
    
    static auto makeExportFuncName (
        const std::string & sModule, const std::string & sFunc ) {

        return makeAPIFuncName(sModule, sFunc, "lmm_", "_export_");
    } // makeExportFuncName

////////////////////////////////////////////////////////////////////////////////

    using API_FUNC_NAME_MAKER_FPTR_T =
        std::string (*)(const std::string &, const std::string &);

    static std::string makeAPIFuncName (
        const std::string & sName, const char * pszPrefix, size_t nPrefixLen,
        const char * pszExt, size_t nExtLen,
        API_FUNC_NAME_MAKER_FPTR_T apiNameMaker ) {

        auto nBeginOffset = sName.find(pszPrefix);
        const auto bNameWithPrefix = (0 == nBeginOffset);
        if(!bNameWithPrefix) {
            return "";
        }
                    
        auto nEndOffset = sName.find(pszExt);
        const auto bNameWithExt = (std::string::npos != nEndOffset);
        if(!bNameWithExt) {
            return "";
        }

        nBeginOffset += nPrefixLen;
        
        auto && sAPIFuncName =
            std::string(sName.c_str() + nEndOffset + nExtLen);

        nEndOffset -= nBeginOffset;
                
        auto && sModuleName =
            std::string(sName.c_str() + nBeginOffset, nEndOffset);

        return apiNameMaker(sModuleName, sAPIFuncName);
    } // makeAPIFuncName

////////////////////////////////////////////////////////////////////////////////

    static auto importAsExportName(const std::string & sName) {
        return makeAPIFuncName ( sName, "lmp_", length("lmp_"),
            "_import_", length("_import_"), &makeExportFuncName );
    } // importAsExportName

////////////////////////////////////////////////////////////////////////////////

    static auto exportAsImportName(const std::string & sName) {
        return makeAPIFuncName ( sName, "lmm_", length("lmm_"),
            "_export_", length("_export_"), &makeImportFuncName );
    } // exportAsImportName

////////////////////////////////////////////////////////////////////////////////

public:

////////////////////////////////////////////////////////////////////////////////

    auto loadLibs() {
        if(!m_dlls.empty()) {
            return true;
        }

        auto bRet = true;
        g_logger.appendAsInfo("loading modules is starting ..", 2);

        auto && lmm = loadModules();

        g_logger.appendAsInfo("loading modules has finished", 2);
        g_logger.appendAsInfo("loading plugins is starting ..", 2);

        auto && lmp = loadPlugins();

        auto lambdaEraseContainer =
            [](auto & mapToClean, auto & itersToRemove) -> void {
            using ITER_T = typename std::remove_reference
                <decltype(mapToClean)>::type::iterator;

            std::sort(itersToRemove.begin(), itersToRemove.end(),
                [](ITER_T & it_1, ITER_T & it_2) -> auto {
                    return (*it_1).first < (*it_2).first;
                }
            );

            itersToRemove.erase     (
                std::unique(itersToRemove.begin(), itersToRemove.end(),
                    [](ITER_T & it_1, ITER_T & it_2) -> auto {
                        return (*it_1).first == (*it_2).first;
                    }),
                itersToRemove.end() );

            for(auto iter : itersToRemove) {
                mapToClean.erase(iter);
            }
        }; // lambdaEraseContainer

        for(auto & module : lmm) {
            std::unordered_map<std::string, void *> remappedKeys;

            auto & moduleAPIFuncsMap = module.second;
            const auto & sModule = module.first;

            // changing naming the 'module(s)'('export') API functions,
            // from for example: "lmm_hypercube_export_*" to
            // "lmp_hypercube_import_*" to map then the addresses to
            // ones related to 'plugin(s)'
            for(const auto & moduleAPI : moduleAPIFuncsMap) {
                const auto pAddr = moduleAPI.second;
                const auto & sFuncName = moduleAPI.first;

                auto && sNewKey = exportAsImportName(sFuncName);
                if(!sNewKey.length()) {
                    break; // error will be generated later
                }

                remappedKeys[std::move(sNewKey)] = pAddr;
            }

            // checking that all of 'export'/'import' names have
            // successfully been mapped
            if(remappedKeys.size() != moduleAPIFuncsMap.size()) {
                g_logger.appendAsError("can't finish initiating: 'lmm_" +
                                       sModule + ".so'; 'lambdamod' "
                                       "will not work ..", 2);
                bRet = false;
                break;
            }

            using PLUGIN_ORIG_CONTAINER_ITER = std::remove_reference
                <decltype(lmp)>::type::iterator;
            using PLUGIN_ITERS_TMP_CONTAINER =
                std::vector<PLUGIN_ORIG_CONTAINER_ITER>;

            PLUGIN_ITERS_TMP_CONTAINER pluginContainerItersToRemove;

            for(auto plugIt = lmp.begin(); plugIt != lmp.end(); ++plugIt) {
                auto & plugin = (*plugIt);
                auto & pluginAPIFuncsMap = plugin.second;

                if(pluginAPIFuncsMap.empty()) {
                    // all plugin's API('import') functions have already
                    // been mapped to related 'modules', so just remove
                    // the plugin from a list
                    pluginContainerItersToRemove.push_back(plugIt);
                    continue;
                }

                using PLUGIN_API_CONTAINER_ITER = std::remove_reference
                    <decltype(pluginAPIFuncsMap)>::type::iterator;
                using PLUGIN_API_ITERS_TMP_CONTAINER =
                    std::vector<PLUGIN_API_CONTAINER_ITER>;

                PLUGIN_API_ITERS_TMP_CONTAINER pluginAPIContainerItersToRemove;
                const auto & sPlugin = plugin.first;

                auto bUnsupportedAPIs = false;
                for(auto pluginApiIt = pluginAPIFuncsMap.begin();
                    pluginApiIt != pluginAPIFuncsMap.end(); ++pluginApiIt) {

                    const auto & pluginAPI = (*pluginApiIt);
                    const auto & sFuncName = pluginAPI.first;

                    if(std::string::npos == sFuncName.find(sModule)) {
                        // plugin's API('import') func. is not
                        // related to a 'module'
                        continue;
                    }

                    auto it = remappedKeys.find(sFuncName);
                    if(remappedKeys.end() == it) {
                        bUnsupportedAPIs = true;
                        break; // error will be generated later
                    }

                    const auto pAddr = pluginAPI.second;

                    using IMPORT_FPTR_T = void (*)(void *);
                    IMPORT_FPTR_T import_fptr =
                        reinterpret_cast<IMPORT_FPTR_T>(pAddr);

                    using EXPORT_FPTR_T = void * (*)(void);
                    EXPORT_FPTR_T export_fptr =
                        reinterpret_cast<EXPORT_FPTR_T>((*it).second);

                    import_fptr(export_fptr());
                    pluginAPIContainerItersToRemove.push_back(pluginApiIt);
                }

                if(bUnsupportedAPIs ||
                        (!pluginAPIContainerItersToRemove.empty()
                        &&
                        (pluginAPIContainerItersToRemove.size() !=
                        remappedKeys.size()))) {

                    auto itToDll = m_dlls.find(sPlugin);
                    if(m_dlls.end() == itToDll) {
                        // it's strange ..
                        throw("m_dlls.end() == itToDll"); // TODO
                    }

                    dlclose((*itToDll).second.first);
                    m_dlls.erase(itToDll);

                    pluginContainerItersToRemove.push_back(plugIt);

                    g_logger.appendAsError("versions of: 'lmm_" +
                        sModule + ".so' & 'lmp_" + sPlugin + ".so' are "
                        "not compatible; plugin will be ignored ..", 4);
                } else {
                    lambdaEraseContainer(pluginAPIFuncsMap,
                                         pluginAPIContainerItersToRemove);
                }
            }

            lambdaEraseContainer(lmp, pluginContainerItersToRemove);
        }

        g_logger.appendAsInfo("loading plugins has finished", 2);

        g_logger.flush();
        return bRet;
    } // loadLibs

////////////////////////////////////////////////////////////////////////////////

    auto getStartPhaseFptrs() {
        using FPTR_T = void (*)(size_t);
        std::vector<FPTR_T> ret;

        for(const auto & pair : m_dlls) {
            ret.push_back(reinterpret_cast<FPTR_T>(pair.second.second));
        }

        return ret;
    } // getStartPhaseFptrs

////////////////////////////////////////////////////////////////////////////////

}; // class CExtensionsLoader

inline CExtensionsLoader g_extensionsLoader;

} // namespace lambdamod
