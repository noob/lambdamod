#pragma once

#include <string>

////////////////////////////////////////////////////////////////////////////////

namespace lambdamod {

class CLogger;
extern inline CLogger g_logger;

class CLogger {
private:

////////////////////////////////////////////////////////////////////////////////

    static constexpr const char * getPrefix() {
        return "\n\n[LAMBDAMOD][CORE]: ";
    } // getPrefix

////////////////////////////////////////////////////////////////////////////////

    static std::string prepareMsg() {
        std::string ret{getPrefix()};
        ret.reserve(1024);
        return ret;
    } // prepareMsg

////////////////////////////////////////////////////////////////////////////////

    std::string m_sMsg{prepareMsg()};

////////////////////////////////////////////////////////////////////////////////

    void append (
        const char * pszPrefix,
        const std::string & sMsg,
        size_t nShift) {
        
        m_sMsg += '\n';
        
        if(!!nShift) {
            m_sMsg += std::string(nShift, '.');
        }

        m_sMsg += pszPrefix;
        m_sMsg += sMsg;
    } // append

////////////////////////////////////////////////////////////////////////////////

public:

////////////////////////////////////////////////////////////////////////////////

    void appendAsError(const std::string & sMsg, size_t nShift = 0u) {
        append("[E]: ", sMsg, nShift);
    } // appendAsError
        
////////////////////////////////////////////////////////////////////////////////
        
    void appendAsWarning(const std::string & sMsg, size_t nShift = 0u) {
        append("[W]: ", sMsg, nShift);
    } // appendAsWarning

////////////////////////////////////////////////////////////////////////////////

    void appendAsInfo(const std::string & sMsg, size_t nShift = 0u) {
        append("[I]: ", sMsg, nShift);
    } // appendAsInfo

////////////////////////////////////////////////////////////////////////////////

    void flush() {
        std::cout << m_sMsg << '\n' << std::endl;
        m_sMsg = prepareMsg();
    } // flush
 
////////////////////////////////////////////////////////////////////////////////
    
}; // class CLogger

inline CLogger g_logger;

} // namespace lambdamod

