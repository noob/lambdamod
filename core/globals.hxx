#pragma once

#include "extdll.h"
#include "meta_api.h"

#include <iostream>

////////////////////////////////////////////////////////////////////////////////

inline meta_globals_t * gpMetaGlobals;
inline gamedll_funcs_t * gpGamedllFuncs;
inline mutil_funcs_t * gpMetaUtilFuncs;
inline plugin_info_t * gpPluginInfo;
inline globalvars_t * gpGlobals;
inline enginefuncs_t g_engfuncs;

inline META_FUNCTIONS g_metaFuncs;
inline plugin_info_t g_pluginInfo;

////////////////////////////////////////////////////////////////////////////////
