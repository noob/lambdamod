#pragma once

#include "extdll.h"
#include "meta_api.h"
#include <string>
#include <fstream>
#include <map>

////////////////////////////////////////////////////////////////////////////////

namespace lambdamod {

class CHostInfoProvider;
extern inline CHostInfoProvider g_hostInfoProvider;

class CHostInfoProvider {
private:

////////////////////////////////////////////////////////////////////////////////

    std::string m_sGameTitle{"NEED TO 'INIT'"};
    std::string m_sPath{"NEED TO 'INIT'"};

    std::map<std::string, std::string> m_namesRoadMap{[]() -> auto {
        decltype(m_namesRoadMap) ret;
        ret["Half-Life"] = "valve";
        return ret;}()};

    std::string m_sCmdLine{[]() -> auto {
        decltype(m_sCmdLine) ret;
        std::ifstream ifstr("/proc/self/cmdline", std::ios_base::in);
        if(ifstr.good()) {
            std::getline(ifstr, ret);
        }
        return ret;}()};

////////////////////////////////////////////////////////////////////////////////

public:

////////////////////////////////////////////////////////////////////////////////

    void init() {
        auto getGameTitleLambda = []() -> auto {
            const std::string sKey =
                gpGamedllFuncs->dllapi_table->pfnGetGameDescription();
            auto & roadMap = g_hostInfoProvider.m_namesRoadMap;
            auto it = roadMap.find(sKey);
            if(roadMap.end() == it) {
                return std::string("valve");
            }
            return (*it).second;};

        m_sGameTitle = getGameTitleLambda();

        auto getPathLambda = []() -> auto {
            std::string s;
            const std::string sPath = getenv("PWD");
            const auto & sGameTitle = g_hostInfoProvider.m_sGameTitle;

            s.reserve(sPath.size() + sGameTitle.size() + 2);
            s += sPath;
            s += '/';
            s += sGameTitle;
            s += '/';
            return s;};
        
        m_sPath = getPathLambda();
    } // init

////////////////////////////////////////////////////////////////////////////////

    auto getPath() const {
        return m_sPath;
    } // getPath

////////////////////////////////////////////////////////////////////////////////

    auto getCmdLine() const {
        return m_sCmdLine; 
    } // getCmdLine

////////////////////////////////////////////////////////////////////////////////

    auto getGameTitle() const {
        return m_sGameTitle;
    }; // getGameTitle

////////////////////////////////////////////////////////////////////////////////

}; // class CHostInfoProvider

inline CHostInfoProvider g_hostInfoProvider;

} // namespace lambdamod

