#!/bin/bash

function clean {
    rm -f Makefile
    rm -f CMakeCache.txt
    rm -f cmake_install.cmake
    rm -rf CMakeFiles
    rm -f lambdamod_core.so
}

clean
cmake CMakeLists.txt
make
rm -f ~/Desktop/hlds/valve/addons/lambdamod/dlls/lambdamod_core.so
cp lambdamod_core.so ~/Desktop/hlds/valve/addons/lambdamod/dlls/
clean
