#!/bin/bash

function clean {
    rm -f Makefile
    rm -f CMakeCache.txt
    rm -f cmake_install.cmake
    rm -rf CMakeFiles
    rm -f lmm_hypercube.so
}

clean
cmake CMakeLists.txt
make
rm -f ~/Desktop/hlds/valve/addons/lambdamod/dlls/lmm_hypercube.so
cp lmm_hypercube.so ~/Desktop/hlds/valve/addons/lambdamod/dlls/
clean
