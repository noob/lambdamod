
#include "CHyperCube.hxx"
#include <iostream>

inline CHyperCube g_hyperCube;

// /////////////////////////////////////////////////////////////////////////////
// [export API]: Begin
// /////////////////////////////////////////////////////////////////////////////

void lambdamod_init(size_t nStartPhase) {
    std::cout << "\n << lambdamod_init ('hypercube' plugin) >>" << std::endl;
}

extern "C" void lambdamod_start_phase(size_t nStartPhase) {
    lambdamod_init(nStartPhase);
}


extern "C" auto lmm_hypercube_export_trackEventByEntTag() {
    using TRACK_FPTR_T = size_t (*)(const char *, size_t, void *, size_t);
    TRACK_FPTR_T fptr = &CHyperCube::trackEvent;
    return fptr;
} // lmm_trackEventByEntTag_export

extern "C" auto lmm_hypercube_export_trackEventByEntId() {
    using TRACK_FPTR_T = size_t (*)(size_t, size_t, void *, size_t);
    TRACK_FPTR_T fptr = &CHyperCube::trackEvent;
    return fptr;
} // lmm_trackEventByEntId_export

extern "C" auto lmm_hypercube_export_hangTracking() {
    return &CHyperCube::hangTracking;
} // lmm_hangTracking_export

extern "C" auto lmm_hypercube_export_unhangTracking() {
    return &CHyperCube::unhangTracking;
} // lmm_unhangTracking_export

extern "C" auto lmm_hypercube_export_isTracked() {
    return &CHyperCube::isTracked;
} // lmm_isTracked_export

// /////////////////////////////////////////////////////////////////////////////
// [export API]: End
// /////////////////////////////////////////////////////////////////////////////

size_t CHyperCube::trackEvent ( const char * pszClassname, etEvents eEvent, CB_T cb, size_t data ) {
                               // size_t eEvent,
                                //void * pHandler,
                                //size_t nUserData ) {
    std::cout << "\n[HYPERCUBE]: 'trackEvent[0]'" << std::endl;
    
    auto & hc = g_hyperCube;
    auto vtable = hc.getVTable(pszClassname);
    auto it = hc.m_dimensions.find(vtable);
    EVENTS_T * pEvents = nullptr;

    if(hc.m_dimensions.end() == it) {
        hc.m_dimensions.insert(std::make_pair(vtable, EVENTS_T{}));
        it = hc.m_dimensions.find(vtable);
        pEvents = &((*it).second);
        auto & rEvents = *pEvents;

        for(auto & subscribers : rEvents) {
            subscribers.reserve(32);
        }
    } else {
        pEvents = &((*it).second);
    }

    auto & events = *pEvents;
    const auto nEventId = static_cast<size_t>(eEvent);
    auto & subscribers = events[nEventId];
    const auto nCurrentQueueSize = subscribers.size();
    const auto nRet = hc.m_eventsLinks.size();

    subscribers.emplace_back(cb, data);
    hc.m_eventsLinks.emplace_back(vtable, nEventId, nCurrentQueueSize);

    return nRet;
} // trackEvent

////////////////////////////////////////////////////////////////////////////////

size_t CHyperCube::trackEvent ( size_t, etEvents, CB_T, size_t ) {
                                //void * pHandler, size_t nUserData ) {
    std::cout << "\n[HYPERCUBE]: 'trackEvent[1]'" << std::endl;
    
    return 1;
} // trackEvent

////////////////////////////////////////////////////////////////////////////////

void CHyperCube::unhangTracking(size_t nEvent) {
    std::cout << "\n[HYPERCUBE]: 'unhangTracking'" << std::endl;
} // unhangTracking

////////////////////////////////////////////////////////////////////////////////

void CHyperCube::hangTracking(size_t nEvent) {
    std::cout << "\n[HYPERCUBE]: 'hangTracking'" << std::endl;
} // hangTracking

////////////////////////////////////////////////////////////////////////////////

size_t CHyperCube::isTracked(size_t nEvent) {
    std::cout << "\n[HYPERCUBE]: 'isTracked'" << std::endl;
    
    return 1;
} // isTracked

////////////////////////////////////////////////////////////////////////////////





//#include <iostreeam>
//#include <vector>
//#include <array>


//#include <unordered_map>
//#include <array>








// main class - hypercube
// api:
// - 
// - 
// - 



/*



namespace lambdamod {

    namespace entity {

        namespace vtable {

            class CDispatcher {
            private:

                ////////////////////////////////////////////////////////////////
    
                class CEntityTypeGenerator final {
                private:
                    static auto & getVTables() {
                        static auto & ret = []() -> auto & {
                            static std::vector<void **> ret;
                            ret.reserve(1024);
                            return ret;} ();   
                        return ret;
                    } // getVTables

                    CEntityTypeGenerator() = delete;
                    ~CEntityTypeGenerator() = delete;

                public:
                
                    static auto getVTableByEntityType(int32_t nType) {
                        return getVTables()[nType];
                    } // getVTableByEntityType

                    static auto setEntityTypeByVTable(void ** vtable) {
                        auto & vtables = getVTables();
                        const auto ret = static_cast<int32_t>(vtables.size());
                        vtables.push_back(vtable);
                        return ret;
                    } // setEntityTypeByVTable
                }; // class CEntityTypeGenerator                
                
                ////////////////////////////////////////////////////////////////

                class CPrivate {
                public:

                    ////////////////////////////////////////////////////////////

                    
                    enum class etVTGenericEvents : int32_t {
                        eSpawn = 0,

                        // other events here ..

                        // do not modify the last 2!
                        eInvalid,
                        eInTotal = eInvalid
                    }; // etVTGenericEvents

                    ////////////////////////////////////////////////////////////

                    static etVTGenericEvents
                    event2generic(etVTEvents eEvent) {
                        static etVTGenericEvents
                        eGenericEvents[to_underlying(etVTEvents::eInTotal)] = {
                            etVTGenericEvents::eSpawn,
                            etVTGenericEvents::eSpawn,
                            //

                            // other events here ..

                        }; // etVTGenericEvents

                        return eGenericEvents[to_underlying(eEvent)];
                    } // event2generic
                    
                    ////////////////////////////////////////////////////////////

                private:

                    ////////////////////////////////////////////////////////////

                    using REGISTERED_EVENTS_PRE = std::vector<size_t>;
                    using REGISTERED_EVENTS_POST = REGISTERED_EVENTS_PRE;
                    using REGISTERED_EVENTS_GENERIC = REGISTERED_EVENTS_POST;

                    using REGISTERED_EVENTS = std::unordered_map<void **,
                        std::array<REGISTERED_EVENTS_GENERIC,
                            to_underlying(etVTEvents::eInTotal)> >;

                    ////////////////////////////////////////////////////////////

                    static decltype(auto)
                    getRegisteredEvents(void) {
                        static REGISTERED_EVENTS events;
                        return (events);
                    } // getRegisteredEvents

                    ////////////////////////////////////////////////////////////

                    //static decltype(auto)
                    //getVTablesAssociations(void) {
                    //    static std::vector<void **> vtables;
                    //    return (vtables);
                    //} // getVTablesAssociations

                    ////////////////////////////////////////////////////////////



                public:

                    ////////////////////////////////////////////////////////////

                    static void *
                    getVTableHandler(void ** vtable, etVTGenericEvents eEvent) {
                        const auto & events = getRegisteredEvents();


                    } // getVTableHandler

                    ////////////////////////////////////////////////////////////

                    static void
                    setVTableHandler(void ** vtable, etVTGenericEvents eEvent) {


                    } // setVTableHandler
                }; // class CPrivate


            public: // private FIXME - return! private



                // TODO - rename function
                static auto addVTableAssociation(void ** vtable) {
                    if(nullptr == vtable) {
                        return -1;
                    }

                    const auto ret =
                        CEntityTypeGenerator::setEntityTypeByVTable(vtable); 
                    
                    auto & registeredEvents = getRegisteredEvents();

                    const auto it = registeredEvents.find(vtable);
                    if(registeredEvents.end() == it) {
                        auto x = std::array<std::tuple<void *, std::vector<size_t>>, 2>();
                        registeredEvents.insert(std::make_pair(vtable, x));

                        // пробовал просто оставлять эту строчку, т.е. 2е
                        // вышестоящие строчки отсутствовали
                        auto & events = registeredEvents[vtable];

                        for(auto & event : events) {
                            // NOTE! pre/post "spawn"(for ex.) - are
                            // different events, even in case of a single
                            // ingame class.

                            constexpr auto nRegistrationsPerEvent = 32u;
                            std::get<1>(event).reserve(nRegistrationsPerEvent);
                        }
                    }

                    return ret;
                } // addVTableAssociation

                // valve - linux
                    // pev 4
                    // base 0x0
                    // spawn 0

                static auto getPevOffset(void) {
                    return 4;
                } // getPevOffset

                static auto getBaseOffset(void) {
                    return 0;
                } // getBaseOffset

                static auto getVTable(const void * pThis) {
                    auto pVoid = const_cast<void *>(pThis);
                    auto pByte = reinterpret_cast<char *>(pVoid);
                    pByte += getBaseOffset();
                    auto pvtable = reinterpret_cast<void ***>(pByte);

                    return *pvtable;
                } // getVTable

                static entvars_t * getPev(void * pThis) {
                    auto * pByte = reinterpret_cast<char *>(pThis);
                    pByte += getPevOffset();
                    entvars_t ** ppEnt = reinterpret_cast<entvars_t **>(pByte);

                    return *ppEnt;
                } // getPev

                static void ** getVTable(const edict_t & pEnt) {
                    return getVTable(pEnt.pvPrivateData);
                } // getVTable

                static void ** getVTable(int32_t id) {
                    return getVTable(*(INDEXENT(id)));
                } // getVTable

                static void ** getVTable(const char * pszName) {
                    void ** vtable = nullptr;
                    edict_t * pEnt = CREATE_ENTITY();

                    if(FNullEnt(pEnt) == false) {
                        CALL_GAME_ENTITY(nullptr, pszName, &(pEnt->v));

                        // pEnt->pvPrivateData == this
                        if(!!(pEnt->pvPrivateData)) {
                            vtable = getVTable(*pEnt);
                            REMOVE_ENTITY(pEnt);
                            pEnt = nullptr;
                        }
                    }

                    return vtable;
                } // getVTable

                static auto getEntityTypeByVTable(void ** vtable) {
                    return addVTableAssociation(vtable);
                } // getEntityTypeByVTable

                static auto getEntityTypeByTag(const char * szTag) {
                    return getEntityTypeByVTable(getVTable(szTag));
                } // getEntityTypeByTag

                static auto getEntityTypeByID(int32_t id) {
                    return getEntityTypeByVTable(getVTable(id));
                } // getEntityTypeByID

                static auto getEntityType(void ** vtable) {
                    return getEntityTypeByVTable(vtable);
                } // getEntityType

                static auto getEntityType(const char * szTag) {
                    return getEntityTypeByTag(szTag);
                } // getEntityType

                static auto getEntityType(int32_t id) {
                    return getEntityTypeByID(id);
                } // getEntityTypeaddVTableAssociation

                class IEventData {
                public:
                    virtual ~IEventData(void) {}

                    virtual void *
                    getHandler(void) const noexcept = 0;

                    virtual bool
                    isEnabled(void) const noexcept = 0;

                    virtual etVTEvents
                    getVTEvent(void) const noexcept = 0;

                    virtual int32_t
                    getUserData(void) const noexcept = 0;

                    virtual size_t
                    getQueueOffset(void) const noexcept = 0;

                    virtual void
                    enableEvent(void) noexcept = 0;

                    virtual void
                    disableEvent(void) noexcept = 0;

                    virtual size_t
                    getVTAssociationOffset(void) const noexcept = 0;
                }; // class IEventData

                class CEventData : public IEventData {
                private:
                    size_t m_nVTAssociationOffset;
                    etVTEvents m_eVTEvent; // TODO - make extra classes
                                           // with corresponding enum values
                    size_t m_nQueueOffset;
                    int32_t m_nUserData;
                    void * m_pHandler; // TODO - make extra classes
                                       // with corresponding handler types
                    bool m_bIsEnabled;

                public:
                    CEventData          (
                        size_t nVTAssociationOffset, etVTEvents eEvent,
                        size_t nQueueOffset, int32_t nUserData,
                        void * pHandler, bool bEnabled   ) :

                        m_nVTAssociationOffset(nVTAssociationOffset),
                        m_eVTEvent(eEvent),
                        m_nQueueOffset(nQueueOffset),
                        m_nUserData(nUserData),
                        m_pHandler(pHandler),
                        m_bIsEnabled(bEnabled)
                    {
                        // void
                    } // CEventData

                    virtual void *
                    getHandler(void) const noexcept override {
                        return m_pHandler;
                    } // getHandler

                    void enableEvent(void) noexcept override {
                        m_bIsEnabled = true;
                    } // enableEvent

                    void disableEvent(void) noexcept override {
                        m_bIsEnabled = false;
                    } // disableEvent

                    bool isEnabled(void) const noexcept override {
                        return m_bIsEnabled;
                    } // isEnabled

                    etVTEvents getVTEvent(void) const noexcept override {
                        return m_eVTEvent;
                    } // getVTEvent

                    int32_t getUserData(void) const noexcept override {
                        return m_nUserData;
                    } // getUserData

                    size_t getQueueOffset(void) const noexcept override {
                        return m_nQueueOffset;
                    } // getQueueOffset

                    size_t
                    getVTAssociationOffset(void) const noexcept override {
                        return m_nVTAssociationOffset;
                    } // getVTableAssociationOffset
                }; // class CEventData

                using EVENTS_DATA_T = std::vector<std::unique_ptr<IEventData> >;

                static decltype(auto) getRegisteredEventsData(void) {
                    static EVENTS_DATA_T data;
                    return (data);
                } // getRegisteredEventsData

                static auto
                isPreEvent(etVTEvents eEvent) {
                    return (((to_underlying(eEvent) + 1) % 2) != 0);
                } // isPreEvent

                static auto
                isPreEvent(size_t nEvent) {
                    return isPreEvent(static_cast<etVTEvents>(nEvent));
                } // isPreEvent

                static void
                gblSpawnHandler(void * pThis) {
                    std::cout << "\nTURANGA_LEELA[02.09.2018] .. before spawn" << std::endl;

                    using api = lambdamod::entity::vtable::CDispatcher;
                    auto vtable = api::getVTable(pThis);
                    auto & eventsOffsets = api::getRegisteredEvents()[vtable]; // эти данные нужно передать

                    auto & preEventsOffsets = std::get<1>(eventsOffsets[to_underlying(etVTEvents::eSpawning)]);
                    auto & postEventsOffsets = std::get<1>(eventsOffsets[to_underlying(etVTEvents::eSpawned)]);

                    auto & data = api::getRegisteredEventsData();

                    auto spawningAPI = __lambdamod::pod::api::SLambdamod::SVtable::SUserCBs::SSpawning();
                    auto spawnedAPI = __lambdamod::pod::api::SLambdamod::SVtable::SUserCBs::SSpawned();

                    for(auto preEventOffset : preEventsOffsets) {
                        auto & event = data[preEventOffset];

                        using PRE_SPAWN = void (*)(__lambdamod::pod::api::SLambdamod::SVtable::SUserCBs::SSpawning *);

                        if(event->isEnabled()) {
                            spawningAPI.userData = event->getUserData();
                            ((PRE_SPAWN)event->getHandler())(&spawningAPI);
                        }
                    } // for

                    g_pSpawnOrig(pThis);

                    for(auto postEventOffset : postEventsOffsets) {
                        auto & event = data[postEventOffset];

                        using POST_SPAWN = void (*)(__lambdamod::pod::api::SLambdamod::SVtable::SUserCBs::SSpawning *);

                        if(event->isEnabled()) {
                            spawnedAPI.userData = event->getUserData();
                            ((POST_SPAWN)event->getHandler())(&spawningAPI);
                        }
                    } // for

                    std::cout << "\nTURANGA_LEELA[02.09.2018] .. after spawn" << std::endl;
                } // gblSpawnHandler

                static void
                displaceVTEvent(void ** vtable, size_t nEventID) {
                    struct SDataMapping {
                        void * pHandler;
                        bool bFirstEncounter;
                    }; // struct SDataMapping

                    // FIXME - dataMap должна быть ещё размножена на кол-во vtable,
                    // а не только на кол-во событий
                    static SDataMapping
                    dataMap[to_underlying(etVTEvents::eInTotal)] = {
                        {reinterpret_cast<void *>(&gblSpawnHandler), true},
                        {reinterpret_cast<void *>(&gblSpawnHandler), true}
                    }; // dataMap

                    if(dataMap[nEventID].bFirstEncounter) {
                        dataMap[nEventID].bFirstEncounter = false;

                        const auto nSecondEventID = isPreEvent(nEventID) ?
                            (nEventID + 1u) : (nEventID - 1u);

                        dataMap[nSecondEventID].bFirstEncounter = false;

                        constexpr auto nSpawnOffset = 0;
                        __lambdamod_vtable_flags_rw((void *)(&vtable[nSpawnOffset]));
                        g_pSpawnOrig = (PPP)vtable[nSpawnOffset];
                        vtable[nSpawnOffset] = (void *)&gblSpawnHandler;
                    }
                } // displaceVTEvent





                










                static auto
                subscribeToEventByEntityType        (
                    int32_t nEntityType,
                    etVTEvents eEvent,
                    void * pHandler,
                    int32_t nUserData   )
                {
                    std::cout << "\n\n GOOD ALL WORKS OK!\n" << std::endl; // FIXME

                    const auto nVTAssociationOffset =
                        static_cast<size_t>(nEntityType);
                    constexpr auto bEventEnabled = true;
                    const auto vtable =
                        CEntityTypeGenerator::getVTableByEntityType(nEntityType);
                    auto & events = getRegisteredEvents()[vtable];
                    const auto nEventOffset =
                        static_cast<size_t>(to_underlying(eEvent));
                    auto & queue = std::get<1>(events[nEventOffset]);
                    const auto nQueueOffset = queue.size();

                    displaceVTEvent(vtable, nEventOffset);

                    auto & eventsData = getRegisteredEventsData();
                    const auto nDataOffset = eventsData.size();
                    queue.push_back(nDataOffset);
                    const auto ret = static_cast<int32_t>(nDataOffset);

                    eventsData.emplace_back (
                        std::make_unique<CEventData>    (
                            nVTAssociationOffset,
                            eEvent,
                            nQueueOffset,
                            nUserData,
                            pHandler,
                            bEventEnabled               ) );

                    return ret;
                } // subscribeToEvent

                // //////////////
                // API section //
                // //////////////

                // TODO:
                // add => 'triggerEvent(int32_t id, etVTEvents eEvent, ARGS .. ?)'
                // add => 'triggerEventChain(int32_t id, etVTEvents eEvent, ARGS .. ?)'

                static auto
                trackEvent   (
                    const char * pszTag,
                    etVTEvents eEvent,
                    void * pHandler,
                    size_t nUserData   )
                {
                    const auto nType = getEntityType(pszTag);

                    return subscribeToEventByEntityType             (
                                nType, eEvent, pHandler, nUserData  );
                } // subscribeToEventByTag


                // good !
                //void trackEvent
                //bool isTracked
               // void hangTracking
                //void unhangTracking
                
                
                

                static size_t trackEvent( size_t id, etHyperCubeEvents eEvent,
                                        void * pHandler, size_t nUserData   )
                {
                    return subscribeToEventByEntityType (
                            getEntityType(id), eEvent, pHandler, nUserData  );
                } // subscribeToEventById

                static void unhangTracking(size_t nEvent) {
                    getRegisteredEventsData()[nEvent]->enableEvent();
                } // enableEvent

                static void hangTracking(size_t nEvent) {
                    getRegisteredEventsData()[nEvent]->disableEvent();
                } // disableEvent

                static size_t isTracked(size_t nEvent) {
                    const auto bResult =
                        getRegisteredEventsData()[nEvent]->isEnabled();
                    return (bResult ? 1 : 0);
                } // isEventEnabled

            public: // FIXME remove public:
                static auto
                getAPI(void) {
                    static __lambdamod::pod::api::SLambdamod::SVtable api {


                    //    [+]GET_ENTITY_TYPE_BY_TAG_FPTR_T getEntityTypeByTag;
                      //  [+]GET_ENTITY_TYPE_BY_ID_FPTR_T getEntityTypeByID;
                     //   [+]SUBSCRIBE_TO_EVENT_BY_ID_FPTR_T subscribeToEventByID;

                     //   [+]SUBSCRIBE_TO_EVENT_BY_ENTITY_TYPE_FPTR_T
                     //       subscribeToEventByEntityType;

                     //   [+]SUBSCRIBE_TO_EVENT_BY_TAG_FPTR_T
                      //      subscribeToEventByTag;

                      //  [+]ENABLE_EVENT_FPTR_T enableEvent;
                      //  [+]DISABLE_EVENT_FPTR_T disableEvent;
                      //  [+]IS_EVENT_ENABLED_FPTR_T isEventEnabled;


                        &getEntityTypeByTag,
                        &getEntityTypeByID,
                        &subscribeToEventById,
                        &subscribeToEventByEntityType,
                        &subscribeToEventByTag,
                        &enableEvent,
                        &disableEvent,
                        &isEventEnabled

                        // add other ptr(s) here ..

                    }; // API

                    return (&api);
                } // getAPI
            }; // class CDispatcher

        } // vtable

    } // entity

} // lambdamod

*/
