#pragma once

namespace lambdamod {

namespace modules {

class hypercube {
public:

    enum class etHyperCubeEvents : size_t {
        eSpawning = 0u,
        eSpawned,
        eThinking
    }; // enum class etHyperCubeEvents

}; // class hypercube

} // namespace modules 

} // namespace lambdamod
