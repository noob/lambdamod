#pragma once

#include "extdll.h"
#include "meta_api.h"
#include <vector>
#include <tuple>
#include <unordered_map>
#include <array>
//#include <numeric_limits>

class CHyperCube {
public:
    using CB_T = void(*)(size_t);

    enum class etEvents : size_t {
        eUnknown = 0,
        eSpawning,
        eSpawned,

        // last element
        eCount = 3
    }; // etHypercubeEvents

    static size_t trackEvent(const char *, etEvents, CB_T, size_t);
    static size_t trackEvent(size_t, etEvents, CB_T, size_t);
    static void unhangTracking(size_t);
    static void hangTracking(size_t);
    static size_t isTracked(size_t);

private:
    // 1st - vtable; 2nd - etHyperCubeEvent; 3rd - hook offset
    using EVENT_LINK_T = std::tuple<void **, size_t, size_t>;
    std::vector<EVENT_LINK_T> m_eventsLinks;

    class CEvent {
    public:
        CEvent(CB_T cb, size_t data) : m_cb(cb), m_data(data) {
            // void
        }

        void trigger() {
            if(isEnabled()) {
                m_cb(m_data);
            }
        } // trigger

        void enable() {
            m_bEnabled = true;
        } // enable

        void disable() {
            m_bEnabled = false;
        } // disable

    private:
        auto isEnabled() const {
            return m_bEnabled;
        }

        bool m_bEnabled{false};
        CB_T m_cb{nullptr};
        size_t m_data{std::numeric_limits<decltype(m_data)>::max()};
    }; // class CEvent


    using VTABLE_T = void **;
    using EVENT_SUBSCRIBERS_T = std::vector<CEvent>;
    static constexpr auto MAX_EVENTS = static_cast<size_t>(etEvents::eCount);
    using EVENTS_T = std::array<EVENT_SUBSCRIBERS_T, MAX_EVENTS>;
    using DIMENSIONS_T = std::unordered_map<VTABLE_T, EVENTS_T>;

    DIMENSIONS_T m_dimensions;


    /*
    auto trackEvent(const char * pszClassname, etEvents eEvent, CB_T cb, size_t data) {
        auto vtable = getVTable(pszClassname);

        auto it = m_dimensions.find(vtable);
        EVENTS_T * pEvents = nullptr;
        if(m_dimensions.end() == it) {
            m_dimensions.insert(std::make_pair(vtable, EVENTS_T{}));
            it = m_dimensions.find(vtable);
            pEvents = &((*it).second);
            auto & rEvents = *pEvents;

            for(auto & subscribers : rEvents) {
                subscribers.reserve(32);
            }
        } else {
            pEvents = &((*it).second);
        }

        auto & events = *pEvents;
        auto & subscribers = events[static_cast<size_t>(eEvent)];

        const auto nCurrentQueueSize = subscribers.size();
        subscribers.emplace_back(cb, data);

        const auto nRet = m_eventsLinks.size();
        m_eventsLinks.emplace_back(vtable, static_cast<size_t>(eEvent), nCurrentQueueSize);

        return nRet;
    } // trackEvent

*/
    // ..
    // TODO - rename function
    static auto addVTableAssociation(void ** vtable) {
        if(nullptr == vtable) {
            return -1; // TODO
        }



        /*
        const auto ret =
            CEntityTypeGenerator::setEntityTypeByVTable(vtable);

        auto & registeredEvents = getRegisteredEvents();

        const auto it = registeredEvents.find(vtable);
        if(registeredEvents.end() == it) {
            auto x = std::array<std::tuple<void *, std::vector<size_t>>, 2>();
            registeredEvents.insert(std::make_pair(vtable, x));

            // пробовал просто оставлять эту строчку, т.е. 2е
            // вышестоящие строчки отсутствовали
            auto & events = registeredEvents[vtable];

            for(auto & event : events) {
                // NOTE! pre/post "spawn"(for ex.) - are
                // different events, even in case of a single
                // ingame class.

                constexpr auto nRegistrationsPerEvent = 32u;
                std::get<1>(event).reserve(nRegistrationsPerEvent);
            }
        }
        */
        return ret;
    } // addVTableAssociation

    // valve - linux
        // pev 4
        // base 0x0
        // spawn 0

    static auto getPevOffset(void) {
        return 4;
    } // getPevOffset

    static auto getBaseOffset(void) {
        return 0;
    } // getBaseOffset

    static auto getVTable(const void * pThis) {
        auto pVoid = const_cast<void *>(pThis);
        auto pByte = reinterpret_cast<char *>(pVoid);
        pByte += getBaseOffset();
        auto pvtable = reinterpret_cast<void ***>(pByte);

        return *pvtable;
    } // getVTable

    static entvars_t * getPev(void * pThis) {
        auto * pByte = reinterpret_cast<char *>(pThis);
        pByte += getPevOffset();
        entvars_t ** ppEnt = reinterpret_cast<entvars_t **>(pByte);

        return *ppEnt;
    } // getPev

    static void ** getVTable(const edict_t & pEnt) {
        return getVTable(pEnt.pvPrivateData);
    } // getVTable

    static void ** getVTable(int32_t id) {
        return getVTable(*(INDEXENT(id)));
    } // getVTable

    static void ** getVTable(const char * pszName) {
        void ** vtable = nullptr;
        edict_t * pEnt = CREATE_ENTITY();

        if(FNullEnt(pEnt) == false) {
            CALL_GAME_ENTITY(nullptr, pszName, &(pEnt->v));

            // pEnt->pvPrivateData == this
            if(!!(pEnt->pvPrivateData)) {
                vtable = getVTable(*pEnt);
                REMOVE_ENTITY(pEnt);
                pEnt = nullptr;
            }
        }

        return vtable;
    } // getVTable

    static auto getEntityTypeByVTable(void ** vtable) {
        return addVTableAssociation(vtable);
    } // getEntityTypeByVTable

    static auto getEntityTypeByTag(const char * szTag) {
        return getEntityTypeByVTable(getVTable(szTag));
    } // getEntityTypeByTag

    static auto getEntityTypeByID(int32_t id) {
        return getEntityTypeByVTable(getVTable(id));
    } // getEntityTypeByID

    static auto getEntityType(void ** vtable) {
        return getEntityTypeByVTable(vtable);
    } // getEntityType

    static auto getEntityType(const char * szTag) {
        return getEntityTypeByTag(szTag);
    } // getEntityType

    static auto getEntityType(int32_t id) {
        return getEntityTypeByID(id);
    } // getEntityType

}; // class CHyperCube

extern inline CHyperCube g_hyperCube;





/*

 static size_t trackEvent( size_t id, etHyperCubeEvents eEvent,
                                        void * pHandler, size_t nUserData   )
                {
                    return subscribeToEventByEntityType (
                            getEntityType(id), eEvent, pHandler, nUserData  );
                } // subscribeToEventById

                static void unhangTracking(size_t nEvent) {
                    getRegisteredEventsData()[nEvent]->enableEvent();
                } // enableEvent

                static void hangTracking(size_t nEvent) {
                    getRegisteredEventsData()[nEvent]->disableEvent();
                } // disableEvent

                static size_t isTracked(size_t nEvent) {
                    const auto bResult =
                        getRegisteredEventsData()[nEvent]->isEnabled();
                    return (bResult ? 1 : 0);
                } // isEventEnabled
                
                */
